<?php
/**
 * @file
 * anki_ui.features.inc
 */

/**
 * Implements hook_views_api().
 */
function anki_ui_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
